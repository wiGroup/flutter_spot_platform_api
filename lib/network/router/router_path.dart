import 'router.dart';

extension SpotPlatformApiNetworkRouterPath on SpotPlatformApiRouter {
  String get path {
    switch (this) {
      case SpotPlatformApiRouter.accountConfirmation:
        return '/v1/account-confirmation/{user_id}';
    }
  }
}
