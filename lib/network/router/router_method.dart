// ignore: import_of_legacy_library_into_null_safe
import 'package:flutter_abstract_network_payload/network/network.dart';
import 'router.dart';

extension SpotPlatformApiRouterMethod on SpotPlatformApiRouter {
  // ignore: missing_return
  HttpMethod get method {
    switch (this) {
      case SpotPlatformApiRouter.accountConfirmation:
        return HttpMethod.get;
    }
  }
}
