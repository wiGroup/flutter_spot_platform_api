// ignore: import_of_legacy_library_into_null_safe
import 'package:flutter_abstract_network_payload/network/network.dart';

abstract class SpotPlatformApiNetworkPayload<T> extends NetworkPayload<T> {
  T responseWithStatus(Map<String, dynamic> json, int statusCode);
}
