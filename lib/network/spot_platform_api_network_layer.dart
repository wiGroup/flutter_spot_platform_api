import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:flutter_abstract_network_payload/network/network.dart';
import '../../constants.dart';
import 'network.dart';

@protected
class SpotPlatformApiNetworkLayer {
  Dio httpClient = Dio(
    BaseOptions(
      /// 5 minutes timeout
      receiveTimeout: 5 * 60 * 1000,
    ),
  );

  Future<T> request<T>(
      String baseUrl,
      String jwtToken,
      SpotPlatformApiRouter router,
      SpotPlatformApiNetworkPayload payload) async {
    var options = Options(
      headers: {
        HttpHeaders.contentTypeHeader: ContentType.json.value,
        HttpHeaders.authorizationHeader: 'JWT ' + jwtToken,
      },
      method: router.method.value,
    );

    String url = _url(
      baseUrl,
      router,
      payload.pathParameters,
      payload.queryParameters,
    ).toString();

    try {
      final response = await httpClient.request(
        url,
        options: options,
      );

      return Future.value(payload.response(response.data));
    } catch (e) {
      return Future.error(GENERAL_ERROR_MESSAGE);
    }
  }

  Uri _url(
    String baseUrl,
    SpotPlatformApiRouter router,
    Map<String, String> pathParameters,
    Map<String, String> queryParameters,
  ) {
    var path = router.path;

    if (pathParameters.isNotEmpty) {
      for (final entry in pathParameters.entries) {
        path = path.replaceAll('{${entry.key}}', entry.value);
      }
    }

    return Uri.https(baseUrl, path, queryParameters);
  }
}
