import '../api_response.dart';

class AccountConfirmationResponse {
  ApiResponse? response;

  AccountConfirmationResponse({required this.response});

  factory AccountConfirmationResponse.fromJson(Map<String, dynamic> json) =>
      AccountConfirmationResponse(
          response: json['response'] != null
              ? ApiResponse.fromJson(json['response'])
              : null);
}
