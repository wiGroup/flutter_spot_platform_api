// ignore: import_of_legacy_library_into_null_safe
import 'package:flutter_abstract_network_payload/network/http_request_algorithm.dart';
import 'package:flutter_spot_platform_api/network/network.dart';

import 'account_confirmation_response.dart';

class AccountConfirmationPayLoad extends SpotPlatformApiNetworkPayload {
  final String userId;

  AccountConfirmationPayLoad({required this.userId});

  @override
  Map<String, String>? get additionalHeaders => null;

  @override
  Map<String, String>? get queryParameters => null;

  @override
  Map<String, dynamic>? request() => null;

  @override
  AccountConfirmationResponse response(Map<String, dynamic> json) =>
      AccountConfirmationResponse.fromJson(json);

  @override
  Map<String, String> get pathParameters => {
        'user_id': userId,
      };

  @override
  AccountConfirmationResponse? responseWithStatus(
          Map<String, dynamic> json, int statusCode) =>
      null;

  @override
  EHttpRequestAlgorithm get requestAlgorithm => EHttpRequestAlgorithm.replace;
}
