class ApiResponse {
  static final int successCode = 0;

  final dynamic code;
  final String desc;

  ApiResponse({required this.code, required this.desc});

  factory ApiResponse.fromJson(Map<String, dynamic> json) {
    return ApiResponse(
      code: json['code'],
      desc: json['desc'],
    );
  }
}
