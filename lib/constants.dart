const GENERAL_ERROR_MESSAGE =
    'Hmmm… there seems to be some trouble processing your request. Please try again.';
