import 'dart:async';

import 'package:flutter_spot_platform_api/models/models.dart';
import 'package:flutter_spot_platform_api/network/network.dart';

class SpotPlatformApi {
  //Account confirmation email submission
  static Future<AccountConfirmationResponse> submitAccountConfirmation(
      {required String baseUrl,
      required String jwtToken,
      required String userId}) async {
    return SpotPlatformApiNetworkLayer()
        .request<AccountConfirmationResponse>(
            baseUrl,
            jwtToken,
            SpotPlatformApiRouter.accountConfirmation,
            AccountConfirmationPayLoad(userId: userId))
        .then((response) {
      if (response.response!.code == ApiResponse.successCode) {
        return Future.value(response);
      }
      throw (response.response!.desc);
    });
  }
}
