import Flutter
import UIKit

public class SwiftFlutterSpotPlatformApiPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "flutter_spot_platform_api", binaryMessenger: registrar.messenger())
    let instance = SwiftFlutterSpotPlatformApiPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    result("iOS " + UIDevice.current.systemVersion)
  }
}
