import 'package:flutter/material.dart';
import 'package:flutter_spot_platform_api/flutter_spot_platform_api.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _response = '';

  //Logic Methods
  _makeApiCall() async {
    setState(() {
      _response = 'Loading...';
    });

    SpotPlatformApi.submitAccountConfirmation(
            baseUrl: 'bank.qaspotplatformapi.com',
            jwtToken:
                'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjlTaEpLREh6UjFDS1dYN3R0U1RacFFYejFRWSJ9.eyJhdWQiOiI2NjNlOTY5Ni1iMDI0LTQ0NjMtYjU4ZC00NGUyMjQxZWMyMDkiLCJleHAiOjE2MzY1NDgxNjYsImlhdCI6MTYzNjQ4ODE2NiwiaXNzIjoidmlyZ2lubW9uZXkuY28uemEiLCJzdWIiOiIwMTRhMGM5Mi04ZGQ4LTRlZTgtODMxZS02YzkzOTIwMjE1NDIiLCJhdXRoZW50aWNhdGlvblR5cGUiOiJQQVNTV09SRCIsImVtYWlsIjoibGVvbmFyZG8ubGVyYXNzZUB5b3lvd2FsbGV0LmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiIyNzc4NjIwMDUwMCIsImFwcGxpY2F0aW9uSWQiOiI2NjNlOTY5Ni1iMDI0LTQ0NjMtYjU4ZC00NGUyMjQxZWMyMDkiLCJyb2xlcyI6W10sInN1YnNjcmliZXJJZCI6IjM5MCIsInN1YnNjcmliZXJTdGF0ZSI6IkEifQ.PU4PrGDfuJyjK20S8_ySVTadMEZ4bSxvv4cxTWc35KL5hZlkMEqpNtX6pzBSC9Hqqt29im6xHkEfGt232lJfEM5e9kEqYSOg2e6QADFR-uaW9_eFAlaonHCK4Axbl55GBJ-On0R7fsCiTQneHKFw2TTOKjvodNIARd_CtzO78PKsIeFX9IK9hf998qLbCRh8Ie7VDuyM586h7FaY6KmQx-NJjeRJi3P4AGHHUNa2RE350DNrYUvU9mOkKCQeVj7ty_MjiIai9cb9tAeMay0qiFvswhWBLtl-4prkpDfvo90_0g52N7o107BAqeBZXgAKwRYL2Pyvr8Rjqol88KePoQ',
            userId: '390')
        .then((res) {
      setState(() {
        _response = res.response!.desc;
      });
    }).onError((error, stackTrace) {
      setState(() {
        _response = error.toString();
      });
    });
  }

  //UI Methods
  _contents() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Expanded(
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                ElevatedButton(
                    onPressed: _makeApiCall, child: Text('Confirm Account')),
                SizedBox(height: 30),
                Expanded(
                    child: Center(
                  child: SingleChildScrollView(
                    child: Text(_response),
                  ),
                )),
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(8),
          color: Colors.black.withOpacity(0.4),
          child: Text(
            'Plugin developped by Leonardo Lerasse',
            style: TextStyle(fontSize: 12),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Spot platform API example app'),
        ),
        body: _contents(),
      ),
    );
  }
}
